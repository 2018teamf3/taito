import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.io.FileInputStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
//がんばりましょう卍卍
public class MainFrame extends JFrame{

	public static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Desktop\\test-67cc6-096b6d45ce16.json"; // 認証情報の JSONファイル名のパスを指定。
	public static String URL = "https://test-67cc6.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。
	
	/*public String[] PanelNames = {"top","where","showmaps","showmaps2","design","pass"};
    Top top = new Top(this,this.PanelNames[0]);
    Where where = new Where(this,this.PanelNames[1]);
    ShowMaps showmaps;
    ShowMaps2 showmaps2;
    Design design = new Design(this,this.PanelNames[4]);
    JScrollPane designscroll = new JScrollPane(design);
    Pass pass;
    */
	
    CardLayout layout= new CardLayout();
    
    JPanel title = new Title(this);
    JPanel top = new Top(this);
    JPanel where = new Where(this);
    JPanel showmaps = new ShowMaps(this);
    JPanel showmaps2 = new ShowMaps2(this,(Where) where);
    JPanel showmaps2_b = new ShowMaps2(this,(Where) where);
    JPanel design = new Design(this);
    JScrollPane designscroll = new JScrollPane(design);
    JPanel pass = new Pass(this);
    JPanel fee = new Fee(this);
    //JPanel panel_main = new JPanel();

    
    public static void main(String[] args) {

    	try {
    		 FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);
             FirebaseOptions options = new FirebaseOptions.Builder()
                 .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                 .setDatabaseUrl(URL)
                 .build();
             FirebaseApp.initializeApp(options);
		} catch (Exception e) {
			e.printStackTrace();
		}

    	JFrame mf = new MainFrame();
    	mf.setVisible(true);
       /* MainFrame mf = new MainFrame();
        mf.setDefaultCloseOperation(EXIT_ON_CLOSE);
        mf.setVisible(true);*/
    }
    
    public MainFrame(){

    	this.setTitle("シェンブレラ");
    	this.setSize(650, 730);
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	this.setLayout(this.layout);
    	this.add(this.title, "Title");
    	this.add(this.top, "Top");
    	this.add(this.where, "Where");
    	this.add(this.showmaps,"ShowMaps");
    	this.add(this.showmaps2_b,"ShowMaps2");
    	this.add(this.designscroll, "Design");
    	this.add(this.pass,"Pass");
    	this.showPanel("Title");
    	this.showmaps2_b.setLayout(new BorderLayout());
    	this.add(this.fee,"Fee");
    }
    /*public void PanelChange(String str){
    	
    	SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				PanelChange_(str);
			}
		});
    
    }
    */
    /*public void PanelChange(String str){

    	//panel_main.removeAll();
    	
    	System.out.println("50");
    	if(str.equals(PanelNames[1])) {
    		System.out.println("65");
    		//this.remove(top);
    		//getContentPane().removeAll();
    		top.setVisible(false);
    		this.add(where);
    		//this.panel_main.add(new JLabel("こんにちは"));
    	}else if(str.equals(PanelNames[2])) {
    		System.out.println("58");
    		where.setVisible(false);
    		 try {
    		        showmaps = new ShowMaps(this,this.PanelNames[2]);
    		        }catch(Exception e) {
    		        }
    		this.add(showmaps);
    	}else if(str.equals(PanelNames[3])) {
    		System.out.println("66");
    		where.setVisible(false);
    		showmaps2 = new ShowMaps2(this,this.PanelNames[3],where);
    		this.add(showmaps2);
    	}else if(str.equals(PanelNames[4])) {
    		System.out.println("71");
    		this.add(designscroll);
    	}
    	else if(str.equals(PanelNames[5])) {
    		System.out.println("75");
    		designscroll.setVisible(false);
    		try {
    			 pass = new Pass(this,this.PanelNames[5]);
    		}catch(Exception e) {
//    			e.printStackTrace();
    		}

    		if(pass!=null) {
    			System.out.println("pass = " + pass);
//    			this.getContentPane().removeAll();
//    			this.setLayout(new GridLayout(1,1));
    			this.add(pass);
    			this.invalidate();
//    			this.add(new JLabel("aaaaa"));
    			System.out.println("89");
    		}
    		System.out.println("91");
	}

    }*/
    public void showPanel(String panel_name)
    {
    	if(panel_name.equals("ShowMaps2"))
    	{
    		showmaps2_b.removeAll();
    		showmaps2_b.add(new ShowMaps2(this, (Where)where));
    	}
    	
    	System.out.println(panel_name);
    	layout.show(getContentPane(), panel_name);
    
}
  
}