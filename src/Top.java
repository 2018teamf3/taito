import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Top extends JPanel
{
	JButton button1 = new JButton("予約");
	JButton button2 = new JButton("予約");
	JButton button3 = new JButton("返却");
	JButton bb = new JButton("←");
	JLabel label1 = new JLabel("傘");
	JLabel label2 = new JLabel("モバイルバッテリー");
	MainFrame mf;
	String str;
	public Top(MainFrame mf)
	{
		this.mf = mf;
		setLayout(null);
		
		bb.setBounds(0, 0, 50,30);
		button1.setBounds(260, 60, 300, 100);
		button2.setBounds(260, 250, 300, 100);
		button3.setBounds(50, 450, 550, 200);
		label1.setBounds(100,15,200,200);
		label2.setBounds(50, 200, 400, 200);
		
		button1.setFont(new Font("メイリオ", Font.BOLD, 15));
		button2.setFont(new Font("メイリオ", Font.BOLD, 15));
		button3.setFont(new Font("メイリオ", Font.BOLD, 15));
	
		label1.setFont(new Font("メイリオ", Font.BOLD, 15));
		label2.setFont(new Font("メイリオ", Font.BOLD, 15));
		
		
		add(bb);
		add(button1);
		add(button2);
		add(button3);
		add(label1);
		add(label2);
		
		button1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Fee");
			}
		});

		button2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Fee");
			}
		});
		button3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Fee");
			}
		});
		bb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Title");
			}
		});
		
		/*button1.setPreferredSize(new Dimension(200, 100));
		button2.setPreferredSize(new Dimension(200,100));
		button3.setPreferredSize(new Dimension(300, 100));
		bb.setPreferredSize(new Dimension(10,10));
		
		
		button1.setFont(new Font("Arial BOLD ITALIC", Font.ITALIC, 15));
		button2.setFont(new Font("HGS創英角ポップ体", Font.BOLD, 15));
		button3.setFont(new Font("HGS創英角ポップ体", Font.BOLD, 15));
		
		JLabel label1 = new JLabel("Umbrella                    ");
		JLabel label2 = new JLabel("モバイルバッテリー");
		
		label1.setFont(new Font("Arial BOLD ITALIC", Font.BOLD, 15));
		label2.setFont(new Font("HGS創英角ポップ体", Font.BOLD, 15));
		
		add(bb);
		add(label1);
		add(button1, BorderLayout.NORTH);
		button1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Where");
			}
		});
		add(label2);
		add(button2, BorderLayout.CENTER);
		button2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Where");
			}
		});
		add(button3, BorderLayout.SOUTH);
		button3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Where");
			}
		});*/

	}
	}
