import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class Title extends JPanel
{

	JButton button1 = new JButton("シェアアンブレラへようこそ！");
	MainFrame mf;
	String str;
	public Title(MainFrame mf)
	{
		setLayout(null);
		this.mf = mf;
		button1.setBounds(150, 200, 350, 200);
		button1.setFont(new Font("メイリオ", Font.PLAIN, 20));

		add(button1);
		button1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Top");
			}
		});

	}
}