import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Fee extends JPanel
{

	JButton button1 = new JButton("１時間１０円");
	JButton button2 = new JButton("２４時間１５０円");
	JButton bb = new JButton("←");
	MainFrame mf;
	public Fee(MainFrame mf)
	{
		this.mf = mf;
		this.setSize(400,400);

		setLayout(null);
		
		button1.setPreferredSize(new Dimension(200, 100));
		button2.setPreferredSize(new Dimension(200,100));
				
		button1.setFont(new Font("ゴシック体", Font.BOLD, 15));
		button2.setFont(new Font("HGS創英角ポップ体", Font.BOLD, 15));
				
		JLabel label1 = new JLabel("料金プラン");
		label1.setFont(new Font("ゴシック体", Font.BOLD, 20));

		label1.setBounds(30, 10, 100, 100);
		
		
		add(label1);
		add(button1);
		add(bb);
		button1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Where");
			}
		});
		add(button2, BorderLayout.SOUTH);
		button2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Where");
			}
		});

		bb.setBounds(0, 0, 50,30);
		bb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Top");
			}
		});
	}
	}
