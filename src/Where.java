import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Where extends JPanel
{
	JButton button1 = new JButton("現在地から検索");
	JButton button2 = new JButton("検索");
	JButton bb = new JButton("←");
	JLabel text1 = new JLabel("傘ステーション選択");
	MainFrame mf;
	String str;
	JComboBox combo;
	int eki;
	public String latitude;
	public String longitude;
	public Where(MainFrame mf)
	{
		this.mf = mf;
		this.setSize(400,400);
		setLayout(null);
		//text1.setVerticalAlignment(JLabel.TOP);
		//text1.setHorizontalAlignment(JLabel.CENTER);
		text1.setFont(new Font("HGゴシック", Font.BOLD, 40));
		text1.setBounds(130,50, 400, 100);
		
		button1.setBounds(80, 200, 500, 100);
		button1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("ShowMaps");
		}
	});
		
		JLabel label = new JLabel("駅名から検索");
		label.setBounds(90, 350, 100, 100);

		String[] combodata = {"駅名選択","東京駅","新宿","渋谷","原宿","池袋",
								"恵比寿","五反田","品川","堀切菖蒲園"};
		
		combo = new JComboBox(combodata);
		combo.setBounds(180, 375, 200, 50);
		combo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eki = combo.getSelectedIndex();
			}
		});
		bb.setBounds(0, 0, 50,30);
		bb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				mf.showPanel("Top");
			}
		});
		button2.setBounds(400, 375, 150, 50);
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(eki == 1) {
					latitude = "35.681167";
					longitude = "139.767052";
					mf.showPanel("ShowMaps2");
				}else if(eki == 2){
					latitude = "35.689383";
					longitude = "139.700692";
					mf.showPanel("ShowMaps2");
				}else if(eki == 3){
					latitude = "35.658034";
					longitude = "139.701636";
					mf.showPanel("ShowMaps2");
				}else if(eki == 4){
					latitude = "35.670229";
					longitude = "139.702698";
					mf.showPanel("ShowMaps2");
				}else if(eki == 5){
					latitude = "35.729503";
					longitude = "139.710900";
					mf.showPanel("ShowMaps2");
				}else if(eki == 6){
					latitude = "35.647156";
					longitude = "139.709739";
					mf.showPanel("ShowMaps2");
				}else if(eki == 7){
					latitude = "35.626159";
					longitude = "139.723602";
					mf.showPanel("ShowMaps2");
				}else if(eki == 8){
					latitude = "35.626156";
					longitude = "139.738602";
					mf.showPanel("ShowMaps2");
				}else if(eki == 9) {
					latitude = "35.747622";
					longitude = "139.827536";
					mf.showPanel("ShowMaps2");
				}
			}
		});
		add(bb);
		add(text1,BorderLayout.NORTH);
		add(button1,BorderLayout.CENTER);
		add(label,BorderLayout.SOUTH);
		add(combo);
		add(button2);
		
	}

}
