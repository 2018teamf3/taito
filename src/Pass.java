import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Pass extends JPanel implements ActionListener{

	//クラス変数を定義
	public static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Desktop\\test-67cc6-096b6d45ce16.json"; // 認証情報の JSONファイル名のパスを指定。
	public static String URL = "https://test-67cc6.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。
	static String key;
	static Object value;
	static String numRd;
	static String numFb;
	
	MainFrame mf;
	String str;

	//インスタンスの生成
	JTextArea text_area = new JTextArea();
	JButton button = new JButton("入力完了");
	JButton bb = new JButton("←");
	JLabel text = new JLabel("暗証番号");
	JLabel num	= new JLabel("1");
	
	//main関数
    public Pass(MainFrame mf){
    	System.out.println("mainメソッドの処理が開始されました。");
    	//JFrameインスタンスを生成し、表示する
    	//JPanel panel = new Pass();
    	//panel.setVisible(true);\
    	this.mf = mf;
    	this.setSize(400,400);
    	setLayout(null);


    		this.text_area.setEditable(false);
    		this.text_area.setText(randon());
    		
    		text.setFont(new Font("HGゴシック", Font.BOLD, 40));
    		num.setFont(new Font("HGゴシック", Font.BOLD, 50));
    		
    		text_area.setBounds(150, 250, 300, 100);
    		button.setBounds(200, 380, 200, 100);
    		bb.setBounds(0, 0, 50, 30);
    		text.setBounds(230, 20, 300, 50);
    		num.setBounds(300, 50, 100, 100);
    		
    		bb.addActionListener(new ActionListener(){
    			public void actionPerformed(ActionEvent e) {
    				mf.showPanel("Top");
    			}
    		});
    		
    		add(text_area);
    		add(button);
    		add(bb);
    		add(text);
    		add(num);
    		//this.text_area.setPreferredSize(new Dimension(200, 260));
    		//this.button.setPreferredSize(new Dimension(200, 100));
    		  // Firebaseからデータ受信を行う

    	    	DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
    	    	//イベントリスナー生成
    	    	ValueEventListener listener = new ValueEventListener() 
    	    	{
    	    		public void onDataChange(DataSnapshot snapshot)
    	    		{
    	    			key   = snapshot.getKey(); //String型
    	    			value = snapshot.getValue(); //Object型
    	    			System.out.println("データを受信しました。key=" + key + ", value=" + value);
    	    	    	//PassクラスのnumFbにFBから受け取ったvalueをセットする。
    	    	    	Pass.setNumFb(value);
    	    	    	System.out.println("numFbには" + Pass.getNumFb() + "がセットされました");
    	    		}
    	    		
    	    		public void onCancelled(DatabaseError error) {
    	    			System.out.println("キャンセルされました。");
    	    		}
    	    	};
    	    	
    	    	reference.child("ConfirmPassword").child("ArduinoPassword").addValueEventListener(listener);
            // 上記のデータの送受信が終わるまで、しばらく時間がかかりますので、10秒間待機します。
            
            
            System.out.println("mainメソッドの処理が終了しました。");
            
    		//setLayout(new BorderLayout());//めいいっぱい広げる

    		//this.numFb = value.toString();
    		this.button.addActionListener(this);// アクションリスナーをボタンに登録する。
    }

    	//ランダムな数字を生成しString型で出力
        public String randon() {
            Random rand = new Random();
            numRd = String.valueOf(1/*rand.nextInt(2)*/);
            return numRd;
        }

        //ボタンが押されたときの処理
    	@Override
    	public void actionPerformed(ActionEvent e) {
    		String text = this.text_area.getText();
    		if (numFb == null) {
    			text = text + "\n少々お待ちください。もう一度クリックお願いします。";
    		} else {
    			if (numFb.equals(numRd) == false) {
    				text = text + "\nパスワードが間違っています。" + "\n入力された数字は" + numFb + "です。正しい値を入力してください。";
    			} else {
    				text = text + "\n認証完了です。入力された数字は" + numFb + "です。";
    				mf.showPanel("Fee");
    			}
    		}
    		this.text_area.setText(text);
    	}

    	//ゲッター
    	public static String getNumFb() {
    		return numFb;
    	}

    	//object型をString型でセッター
    	public static void setNumFb(Object value) {
    		Pass.numFb = value.toString();
    	}
    
    }