import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class ShowMaps extends JPanel
{
  static String JSON_FILE_NAME = "C:\\Users\\ctctime24\\Desktop\\test-67cc6-096b6d45ce16.json"; // 認証情報の JSONファイル名のパスを指定。
  static String URL = "https://test-67cc6.firebaseio.com/"; // Firebase上のリアルタイムデータベースのURLを指定。
  static String latitude;
  static String longitude;
  static String station;
  JComboBox combo;
  ImageIcon icon = new ImageIcon("C:\\Users\\ctctime24\\Desktop\\画像\\youkai_karakasakozou.png");
  JButton bb = new JButton("←");
  MainFrame mf;
  String str;

 public ShowMaps(MainFrame mf)
  {
	 this.mf = mf;
	 setLayout(null);
    System.out.println("mainメソッドの処理が開始されました。");
    bb.setBounds(0, 0, 50,30);
    add(bb);
    bb.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			mf.showPanel("Where");
		}
	});
    
    String[] combodata = {"1","2","3"};

    combo = new JComboBox(combodata);
    combo.setBounds(30, 620,100,50);
    add(combo);
    // 初期化。

    /*FileInputStream serviceAccount = new FileInputStream(JSON_FILE_NAME);

    FirebaseOptions options = new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl(URL)
        .build();

    FirebaseApp.initializeApp(options);*/

    // Firebase へのデータ送受信を行うサンプル

//    	sampleReceivingDataFromFirebase1(); // Firebase からデータを受信するサンプル1
    DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

    ValueEventListener listener = new ValueEventListener()
    {
      @Override
      public void onDataChange(DataSnapshot snapshot)
      {
        String key   = snapshot.getKey();
        Object value = snapshot.getValue();
        System.out.println("データを受信しました。" + key + "=" + value);
        if (key=="LNG") {
        	longitude=value.toString();
            System.out.println(longitude);
        }else {
        	if (key=="LAT") {
        		latitude=value.toString();
        		System.out.println(latitude);
        	}
        }
      }

      @Override
      public void onCancelled(DatabaseError error)
      {
        System.out.println("キャンセルされました。");
      }
    };

    reference.child("GPS").child("LNG").addValueEventListener(listener);
    reference.child("GPS").child("LAT").addValueEventListener(listener);

    // 上記のデータの送受信が終わるまで、しばらく時間がかかりますので、10秒間待機します。


    System.out.println("mainメソッドの処理が終了しました。");

    while (latitude == null || longitude == null) {
    	
    }
  try {
//		DataAcquisition sample= new DataAcquisition();
//		String latitude
//		String latitude = ShowMaps.latitude;
//		String longitude = ShowMaps.longitude;
		String imageUrl = "https://maps.googleapis.com/maps/api/staticmap?center="+ latitude+ ","+ longitude+ "&zoom=15&size=306x306&scale=2&maptype=roadmap";
		String destinationFile = "image.jpg";


		// read the map image from Google
		// then save it to a local file: image.jpg

		System.out.println(latitude);
		URL url = new URL(imageUrl);
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(destinationFile);
		byte[] b = new byte[2048];


		int length;
		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();
	} catch (IOException e) {
		e.printStackTrace();
		System.exit(1);
	}

	// create a GUI component that loads the image: image.jpg


	ImageIcon imageIcon = new ImageIcon((new ImageIcon("image.jpg")).getImage().getScaledInstance(580, 550,java.awt.Image.SCALE_SMOOTH));
	JLabel Icon = new JLabel(imageIcon);
	Icon.setBounds(30, 50, 580, 550);
	this.add(Icon);
	// ラベルのインスタンスを生成
	JLabel label = new JLabel("Google Map");
	label.setFont(new Font("Century", Font.ITALIC, 24));
	// ボタンのインスタンスを生成
	JButton button = new JButton("デザイン選択");
	button.setFont(new Font("ＭＳゴシック", Font.BOLD, 20));
	button.setBounds(180, 620, 400, 50);
	button.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			mf.showPanel("Design");
		}
	});
	
	/*Container contentPane = this.getContentPane();
	// ラベルをContentPaneに配置
	contentPane.add(label, BorderLayout.NORTH);
	// ボタンをContentPaneに配置
	contentPane.add(button, BorderLayout.SOUTH);*/
	add(button);
	this.setVisible(true);
	}
}
